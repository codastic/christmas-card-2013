
function show(el) {
    $(el).addClass('show');
    setTimeout(function() {
        $(el).addClass('show-active');
    }, 0);
}

function hide(el) {
    $(el).addClass('hide');
    setTimeout(function() {
        $(el).addClass('hide-active');
    }, 0);
}

function step1() {
    show($('main > section:nth-child(1)'));
}

function step2() {
    hide($('main > section:nth-child(1)'));
    show($('main > section:nth-child(2)'));

    var snowFalls = [];
    $('canvas').each(function() {
        this.width = 960;
        this.height = 540;

        var snow = new SnowFall(this, 50);
        snow.start();

        snowFalls.push(snow);
    });

    $('main > section:nth-child(2)').on('mousemove', function(e) {
        for(var i = 0; i < snowFalls.length; i++) {
            snowFalls[i].mX = e.offsetX;
            snowFalls[i].mY = e.offsetY;
        }
    });
}

function step3() {
    var articles = $('main > section:nth-child(2) > article');
    var i = -1;
    var next = function() {
        if(i >= 0) {
            hide(articles[i]);
        }

        i++;
        show(articles[i]);

        if(i < articles.length-1) {
            setTimeout(next, $(articles[i]).data('duration'));
        }
    };

    next();
}

$(function() {
    setTimeout(step1, 2000);
    setTimeout(step2, 7000);
    setTimeout(step3, 11000);
});
