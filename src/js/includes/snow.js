(function() {
    var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame ||
    function(callback) {
        window.setTimeout(callback, 1000 / 60);
    };
    window.requestAnimationFrame = requestAnimationFrame;
})();

(function($) {

    function SnowFall(canvas, flakeCount) {
        this.canvas = canvas;
        this.ctx = canvas.getContext("2d");
        this.flakeCount = flakeCount || 200;
        this.mX = -100;
        this.mY = -100;
    }

    SnowFall.prototype.start = function() {
        this.flakes = [];

        for(var i = 0; i < this.flakeCount; i++) {
            var x = Math.floor(Math.random() * this.canvas.width),
                y = Math.floor(Math.random() * this.canvas.height),
                size = (Math.random() * 3) + 2,
                speed = (Math.random() * 1) + 0.5,
                opacity = (Math.random() * 0.5) + 0.3;

            this.flakes.push({
                speed: speed,
                velY: speed,
                velX: 0,
                x: x,
                y: y,
                size: size,
                stepSize: (Math.random()) / 30,
                step: 0,
                angle: 180,
                opacity: opacity
            });
        }

        this.update();
    };

    SnowFall.prototype.update = function() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

        for(var i = 0; i < this.flakeCount; i++) {
            var flake = this.flakes[i],
                x = this.mX,
                y = this.mY,
                minDist = 150,
                x2 = flake.x,
                y2 = flake.y;

            var dist = Math.sqrt((x2 - x) * (x2 - x) + (y2 - y) * (y2 - y)),
                dx = x2 - x,
                dy = y2 - y;

            if (dist < minDist) {
                var force = minDist / (dist * dist),
                    xcomp = (x - x2) / dist,
                    ycomp = (y - y2) / dist,
                    deltaV = force / 2;

                flake.velX -= deltaV * xcomp;
                flake.velY -= deltaV * ycomp;

            }
            else {
                flake.velX *= 0.98;
                if (flake.velY <= flake.speed) {
                    flake.velY = flake.speed;
                }
                flake.velX += Math.cos(flake.step += 0.05) * flake.stepSize;
            }

            this.ctx.fillStyle = "rgba(255,255,255," + flake.opacity + ")";
            flake.y += flake.velY;
            flake.x += flake.velX;

            if(flake.y >= this.canvas.height || flake.y <= 0) {
                this.reset(flake);
            }


            if(flake.x >= this.canvas.width || flake.x <= 0) {
                this.reset(flake);
            }

            this.ctx.beginPath();
            this.ctx.arc(flake.x, flake.y, flake.size, 0, Math.PI * 2);
            this.ctx.fill();
        }

        var that = this;
        requestAnimationFrame(function() {
            that.update();
        });
    };

    SnowFall.prototype.reset = function(flake) {
        flake.x = Math.floor(Math.random() * this.canvas.width);
        flake.y = 0;
        flake.size = (Math.random() * 3) + 2;
        flake.speed = (Math.random() * 1) + 0.5;
        flake.velY = flake.speed;
        flake.velX = 0;
        flake.opacity = (Math.random() * 0.5) + 0.3;
    };

    window.SnowFall = SnowFall;

}(jQuery));
