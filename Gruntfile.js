/*global module:false*/
module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
                '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
                '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
                '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %> */\n',
        concat: {
            options: {
                separator: ';\n'
            },
            dist: {
                src: [
                    'src/js/vendor/*.js',
                    'src/js/includes/*.js',
                    'src/js/main.js'
                    ],
                dest: 'all.js'
            }
        },
        sass: {
            dist: {
                files: {
                    'all.css': 'src/sass/all.scss'
                }
            }
        },
        autoprefixer: {
            options: {
                browsers: ['last 2 versions', 'ie 9']
            },
            dist: {
                files: {
                    'all.css': 'all.css'
                }
            }
        },
        csso: {
            options: {
                banner: '<%= banner %>'
            },
            dist: {
                files: {
                    'all.css': 'all.css'
                }
            }
        },
        watch: {
            sass: {
                files: ['src/sass/**/*.scss'],
                tasks: ['css']
            },
            js: {
                files: [
                    'src/js/main.js',
                    'src/js/includes/**/*.js',
                    'src/js/vendor/**/*.js'],
                tasks: ['js']
            }
        }
    });

    grunt.loadNpmTasks('grunt-csso');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');

    grunt.registerTask('css', ['sass', 'autoprefixer', 'csso']);
    grunt.registerTask('js', ['concat']);
    grunt.registerTask('default', ['css', 'js']);
};